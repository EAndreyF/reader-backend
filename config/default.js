module.exports = {
  host: 'http://localhost:8003',
  frontPort: 8003,
  cookieSecret: 'reader-backend',
  dbConnectionUrl: 'mongodb://localhost:27017/reader-backend'
};
