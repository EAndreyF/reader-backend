module.exports = {
  host: 'https://ea-reader-backend.herokuapp.com',
  frontPort: process.env.PORT,
  cookieSecret: process.env.COOKIE_SECRET,
  dbConnectionUrl: process.env.MONGODB_URI
};
