var express = require('express');
var app = express();
var config = require('config');
var expressCookieParser = require('cookie-parser');
var methodOverride = require('method-override');
var expressSession = require('express-session');
var bodyParser = require('body-parser');
var MongoStore = require('connect-mongo')(expressSession);
var passport = require('passport');
var mongoose = require('mongoose');

var User = require('./server/models/user');

var EXPRESS_SID_KEY = 'connect.sid';
var COOKIE_SECRET = config.get('cookieSecret');
var cookieParser = expressCookieParser(COOKIE_SECRET);

var sessionStore = new MongoStore({
  mongooseConnection: mongoose.connection
});

var session = expressSession({
  secret: COOKIE_SECRET,
  resave: true,
  saveUninitialized: true,
  store: sessionStore,
  name: EXPRESS_SID_KEY
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser);
app.use(methodOverride());
app.use(session);

app.use(passport.initialize());
app.use(passport.session());

passport.use(User.createStrategy());
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

app.use('/api', require('./server/routes/security'));
app.use('/', function (req, res) {
  res.end('Reader server');
});

app.use(function (err, req, res, next) {
  if (err) {
    return res.send({
      status: 'error',
      message: err + '123'
    });
  } else {
    next();
  }
});

app.use(function (req, res) {
  res.sendStatus(404);
});

module.exports = app;
