var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var passportLocalMongoose = require('passport-local-mongoose');

var User = new Schema({
  roles: {
    type: Array,
    default: ['user']
  },

  isBlocked: {
    type: Boolean,
    default: false
  }
});

/**
 * Check if the user is an administrator
 *
 * @return {Boolean}
 */
User.methods.isAdmin = function () {
  return this.hasRole('admin');
};

/**
 * Check if the user has required role
 *
 * @param {String} role
 * @return {Boolean}
 */
User.methods.hasRole = function (role) {
  return this.roles.indexOf(role) !== -1;
};

/**
 * Serialized only necessary fields
 * @returns {object}
 */
User.methods.toJSON = function () {
  return {
    _id: this.id,
    username: this.username,
    roles: this.roles,
    isBlocked: this.isBlocked
  };
};

User.plugin(passportLocalMongoose);

/**
 * Don't authenticate blocked users
 */
User.statics.authenticate = (function () {
  var base = User.statics.authenticate;
  return function () {
    base = base.call(this);
    return function (username, password, cb) {
      base(username, password, function (err, user, info) {
        if (err || !user) {
          return cb(err, user, info);
        }

        if (user.isBlocked) {
          return cb(null, false, {message: 'User blocked', name: 'UserBlockedError'});
        } else {
          return cb(err, user, info);
        }
      });
    };
  };
})();

module.exports = mongoose.model('User', User);

