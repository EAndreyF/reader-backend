var mongoose = require('mongoose');
var config = require('config');

var url = config.get('dbConnectionUrl');

var deferred = new Promise(function (resolve, reject) {
  mongoose.connect(url);

  var db = mongoose.connection;

  db.on('error', console.error.bind(console, 'connection error:'));
  db.once('open', function() {
    console.log('DB connected');
    resolve(db);
    // we're connected!
  });

});

module.exports = deferred;
