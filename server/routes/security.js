var app = require('express').Router();

app.use('/user', require('./user'));

module.exports = app;
