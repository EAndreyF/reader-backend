var passport = require('passport');
var User = require('../models/user');
var router = require('express').Router();

module.exports = router;

router.get('/logout', logout);
router.post('/signUp', signUp);
router.post('/signIn', signIn);
router.get('/me', me);

function logout(req, res) {
  req.logout();
  res.send();
}

function signUp(req, res, next) {
  var newUser = new User({username: req.body.username});
  User.register(newUser, req.body.password, function (err, user) {
    if (err) {
      return res.send({
        status: 'error',
        message: err.message
      });
    }

    req.logIn(user, function (err) {
      if (err) {
        return next(err);
      }
      return res.send({user: user});
    });
  });
}

function signIn(req, res, next) {
  passport.authenticate('local', function (err, user, info) {
    if (err) {
      return next(err);
    }
    if (!user) {
      return res.send({
        status: 'error',
        message: info.message
      });
    }
    req.logIn(user, function (err) {
      if (err) {
        return next(err);
      }
      return res.send({user: user});
    });
  })(req, res, next);
}

function me(req, res) {
  res.send({user: req.user});
}
