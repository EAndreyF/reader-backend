var mongo = require('./server/mongo');
var server = require('./serve');
var config = require('config');

module.exports = Promise.all([new Promise(function (resolve, reject) {
  server.listen(config.get('frontPort'), function () {
    //var port = server.address().port;
    console.log('Example app listening at http://localhost:%s', config.get('frontPort'));
    resolve(server);
  });
}), mongo]);

